pcspkr.js - "PC Speaker"-like beeps for javascript.

Copyright (C) Alex Richman <alex@richman.io>.

Released as free and open-source software under the ISC licence.


---

### Why?
Why not?!


---

### Example Usage

```javascript
    var pcspkr = new PCSpkr();

    pcspkr.beep(440, 1000); // Beep at 440hz, for 1 second.

    pcspkr.beep_sequence([ // Beep a sequence
        [392,   350],
        [392,   350],
        [392,   350],
        [311.1, 250],
        [466.2, 25],
        [392,   350],
        [311.1, 250],
        [466.2, 25],
        [392,   700],
    ]);
```
