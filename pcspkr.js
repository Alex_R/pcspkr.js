"use strict";


function PCSpkr() {
    /* PCSpkr
     * PCSPKR functionality for javascript.
     */

    this.audio_context = new (window.AudioContext || window.webkitAudioContext)();

    this.gain_node = this.audio_context.createGain();
    this.gain_node.connect(this.audio_context.destination);

    this.gain_node.gain.value = 0.1;

    this.waveform_type = "triangle";


    this._validate_waveform_type = function(waveform_type) {
        if(typeof(waveform_type) != "string") {
            throw TypeError("Argument 'waveform_type' is not of type 'string'");
        }

        if(["sine", "square", "sawtooth", "triangle"].indexOf(waveform_type) == -1) {
            throw TypeError("Argument 'waveform_type' is not valid (must be sine, square, sawtooth, or triangle)");
        }
    }

    this._validate_gain = function(gain) {
        if(typeof(gain) != "number") {
            throw TypeError("Argument 'gain' is not of type 'number'");
        }

        if(gain < -1 || gain > 1) {
            throw TypeError("Argument 'gain' is not valid (must be > -1 and < 1)");
        }
    }

    this._validate_frequency = function(frequency) {
        if(typeof(frequency) != "number") {
            throw TypeError("Argument 'frequency' is not of type 'number'");
        }
    }

    this._validate_duration = function(duration) {
        if(typeof(duration) != "number") {
            throw TypeError("Argument 'duration' is not of type 'number'");
        }

        if(duration < 0) {
            throw TypeError("Argument 'duration' is not valid (must be > 0)");
        }
    }

    this._validate_callback = function(callback) {
        if(typeof(callback) != "function") {
            throw TypeError("Argument 'callback' is not of type 'function'");
        }
    }


    this.set_waveform_type = function(waveform_type) {
        /* set_waveform_type
         * Set the waveform type for future beeps (that don't explicitly specify a waveform type).
         *
         * @ str waveform_type - The type of the waveform to set.  Can be sine, square, sawtooth, or triangle.
         */

        this._validate_waveform_type(waveform_type);

        this.waveform_type = waveform_type;
    }

    this.set_gain = function(gain) {
        /* set_gain
         * Set the gain for future beeps.
         *
         * @ int gain - The gain to set.
         */

        this._validate_gain(gain);

        this.gain_node.gain.value = gain;
    }


    this.beep = function(frequency, duration, waveform_type, callback) {
        /* beep
         * Beep at the supplied frequency for the supplied duration.
         *
         * @ int frequency     - The frequency to beep at hertz.
         * @ int duration      - The duration to beep for in mili-seconds.
         * @ str waveform_type - The type of the waveform to beep, uses the default if not supplied.
         * @ func callback     - A function to be called after the beep is finished, not required.
         */

        this._validate_frequency(frequency);
        this._validate_duration(duration);

        if(typeof(waveform_type) == "undefined") {
            waveform_type = this.waveform_type;
        }
        this._validate_waveform_type(waveform_type);

        if(typeof(callback) != "undefined") {
            this._validate_callback(callback);
        }

        var oscillator = this.audio_context.createOscillator();

        oscillator.connect(this.gain_node);

        oscillator.type = waveform_type;
        oscillator.frequency.value = frequency;

        oscillator.start(0);

        setTimeout(function() {
            oscillator.stop();

            if(typeof(callback) != "undefined") {
                callback();
            }
        }.bind(oscillator, callback), duration);
    }

    this.beep_sequence = function(beeps, callback) {
        /* beep_sequence
         * Beep the supplied beeps.
         *
         * @ array beeps   - An array of tuples of beep() parameters (except for callback).  See beep() for details.
         * @ func callback - A function to be called after the beeps are finished, not required.
         */

        if(typeof(beeps) != "object") {
            throw TypeError("Argument 'beeps' is not of type 'object'");
        }

        if(Object.prototype.toString.call(beeps) != "[object Array]") {
            throw TypeError("Argument 'beeps' is not of type 'array'");
        }

        if(typeof(callback) != "undefined") {
            this._validate_callback(callback);
        }

        /* Validate first, so we don't play half the beeps then throw. */
        for(var iter = 0; iter < beeps.length; iter++) {
            var beep = beeps[iter];

            if(typeof(beep) != "object") {
                throw TypeError("Argument 'beeps' item index '"+ iter.toString() +"' is not of type 'object'");
            }

            if(Object.prototype.toString.call(beep) != "[object Array]") {
                throw TypeError("Argument 'beeps' item index '"+ iter.toString() +"' is not of type 'array'");
            }

            if(beep.length < 2) {
                throw TypeError("Argument 'beeps' item index '"+ iter.toString() +"' does not have enough members");
            }

            try {
                this._validate_frequency(beep[0]);
                this._validate_duration(beep[1]);

                if(beep.length > 2) {
                    this._validate_waveform_type(beep[2]);
                }
            } catch(error) {
                throw TypeError("Argument 'beeps' item index '"+ iter.toString() +"': "+ error.toString());
            }
        }

        function _iter_beep() {
            if(iter >= beeps.length) {
                if(typeof(callback) != "undefined") {
                    callback();
                }

                return;
            }

            var beep = beeps[iter];

            var frequency     = beep[0];
            var duration      = beep[1];
            var waveform_type = undefined;

            if(beep.length > 2) {
                waveform_type = beep[2];
            }

            iter = iter + 1;
            this.beep(frequency, duration, waveform_type, _iter_beep.bind(this, _iter_beep, iter, beeps, callback));
        }

        var iter = 0;
        _iter_beep.call(this, _iter_beep, iter, beeps, callback);
    }
}
